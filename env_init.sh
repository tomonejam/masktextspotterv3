#!/bin/bash

# Install dependencies for apex
apt update && apt install -y cuda-cublas-dev-10-0 cuda-cusparse-dev-10-0

# Install PyTorch with CUDA 10.0 support
pip install torch==1.4.0 torchvision==0.5.0 -f https://download.pytorch.org/whl/cu100/torch_stable.html

# Install pre-built apex
pip install --force-reinstall ./apex-0.1-cp36-cp36m-linux_x86_64.whl
